import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import * as form from "../../files/form.json"


@Component({
  selector: 'app-element',
  templateUrl: './element.component.html',
  styleUrls: ['./element.component.scss']
})
export class ElementComponent implements OnInit {
  public  form: FormGroup;
  public fields: any = (form as any).default;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      skillsArray: this.formBuilder.array([])
    });
    this.fields.forEach(field => {
      let value = null;
      if (field.type === 'select') {
        const selected = field.choices.find(item => item.selected);
        if (selected) {
          value = selected.value;
        }
      }
      const validators = [];
      if (field.required) {
        validators.push(Validators.required);
      }
      const control = new FormControl(value, validators);
      this.form.addControl(field.name, control);
    });
  }

  submit() {
    const checkArray: FormArray = this.form.get('skillsArray') as FormArray;
    checkArray.controls = [];
    this.fields.forEach(field => {
      let value = null;
      if (field.type === 'checkbox') {
        field.checkboxes.forEach(checkbox => {
          if (checkbox.checked) {
            checkArray.push(new FormControl(checkbox.name));
          }
        });
      }
    });
    console.log(this.form.getRawValue());
  }

}
