import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-test-input',
  templateUrl: './test-input.component.html',
  styleUrls: ['./test-input.component.scss']
})
export class TestInputComponent implements OnInit {

  @Input() name;
  @Input() span;
  @Input() parent: FormGroup;
  @Input() addInput;
  @Input() addName;
  @Input() placeholder;

  form: FormGroup;
  productForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.productForm = this.fb.group({
      studies: this.fb.array([]),
    });
  }


  ngOnInit(): void {
  }

  studies(): FormArray {
    return this.productForm.get('studies') as FormArray;
  }

  newStudy(): FormGroup {
    return this.fb.group({
      study: '',
    });
  }

  addStudy() {
    this.studies().push(this.newStudy());
  }

  onSubmit() {
    console.log(this.productForm.getRawValue());
  }

}
