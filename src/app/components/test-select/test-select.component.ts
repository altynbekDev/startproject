import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';


@Component({
  selector: 'app-test-select',
  templateUrl: './test-select.component.html',
  styleUrls: ['./test-select.component.scss']
})
export class TestSelectComponent implements OnInit {


  @Input() parent: FormGroup;
  @Input() name: string;
  @Input() choices: any[];

  constructor() {
  }

  ngOnInit(): void {

  }

}
