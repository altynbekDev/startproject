import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-test-checkbox',
  templateUrl: './test-checkbox.component.html',
  styleUrls: ['./test-checkbox.component.scss']
})
export class TestCheckboxComponent implements OnInit {
  @Input() parent: FormGroup;
  @Input() name: string;
  @Input() checkboxes: any[];

  masterSelected: boolean;

  constructor() {
    this.masterSelected = false;
  };

  ngOnInit(): void {
  }

  select(id) {
    let item = this.checkboxes.find(x => x.id == id);
    if (item) {
      item.checked = !item.checked;
    }
  }

  checkUncheckAll() {
    for (var i = 0; i < this.checkboxes.length; i++) {
      this.checkboxes[i].checked = this.masterSelected;
    }
  }

}
