import {Injectable} from '@angular/core';

@Injectable({
  providedIn: "root"
})

export class TestForm {
  Name: string;
  Age: number;
}
