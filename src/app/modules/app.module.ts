import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from '../components/app/app.component';
import {ElementComponent} from '../components/element/element.component';
import {TestInputComponent} from '../components/test-input/test-input.component';
import {TestSelectComponent} from '../components/test-select/test-select.component';
import {TestNumberComponent} from '../components/test-number/test-number.component';
import {TestCheckboxComponent} from '../components/test-checkbox/test-checkbox.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ElementComponent,
    TestInputComponent,
    TestSelectComponent,
    TestNumberComponent,
    TestCheckboxComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
